<?php
// Vérification de la présence de l'image ainsi que de la présence d'érreurs
if(isset($_FILES['image']) && $_FILES['image']['error'] === 0){
    // Vérification de la taille limite pour l'image.
    if ( $_FILES['image']['size'] <= 3000000){

        $informationsImage = pathinfo($_FILES['image']['name']);
        $extensionImage    = $informationsImage['extension'];
        $extensionsArray   = ['png', 'gif', 'jpg', 'jpeg'];
        // Vérification de la validité de l'extension
        if(in_array($extensionImage, $extensionsArray)) {
            // Réécriture du nom de l'image afin d'éviter les problème d'éccrasement de fichier à l'upload.
            $newImageName = time().rand().rand().'.'.$extensionImage;
            move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/'.$newImageName);
            header('Location: index.php?uploadedFile='.$newImageName);
            exit();
        }
    }
}
header('Location: index.php');