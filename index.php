<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/default.css">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
        <title>ShareFiles - Hébergez gratuitement vos images et en illimité</title>
    </head>
    <body>

        <header>
            <a href="../">
                <span>ShareFiles</span>
            </a>
        </header>

        <section>
            <h1>
            <?php
             if( isset($_GET['uploadedFile']) && file_exists('uploads/'.$_GET['uploadedFile'])) {
                $imagePath = 'uploads/'.$_GET['uploadedFile'];
                echo '<img src="'.$imagePath.'" alt="Image téléchargé" style="max-width : 75%"/>';
             } else {
                echo '<i class="fas fa-paper-plane"></i>';
             }
                
            ?>
            </h1>

            <?php if( isset($_GET['uploadedFile']) && file_exists('uploads/'.$_GET['uploadedFile'])) {?>

            <h2> Fichier envoyé avec succès </h2>
            <p> Retrouvez ci-dessous le lien vers votre fichier</p>
            <input type="text" id="link" value="http://localhost/php-file-storage/uploads/ <?= $imagePath ?>" readonly/>
            <a href="index.php" class="return-link"> Uploader une autre image? </a>
            <?php } else {?>
                <form method="post" action="uploadManager.php" enctype="multipart/form-data">
                    <p>
                        <label for="image">Sélectionnez votre fichier</label><br>
                        <input type="file" name="image" id="image">
                    </p>
                    <p id="send">
                    <button type="submit">Envoyer <i class="fas fa-long-arrow-alt-right"></i></button>
                    </p>
                </form>     
            <?php }?>
        </section>
        
    </body>
</html>